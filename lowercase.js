module.exports = function lowercase(str) {
  try {
    return str.toLowerCase();
  } 
  catch(e) {
    throw new Error('bad input');
  }    
};