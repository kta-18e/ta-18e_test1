const toNumber = require('./toNumber');

describe('toNumber', () => {
  test('1 => 1', () => {
    expect(toNumber(1)).toBe(1);
  });

  test('-1 => -1', () => {
    expect(toNumber('-1')).toBe(-1);
  });

  test('"1" => 1', () => {
    expect(toNumber('1')).toBe(1);
  });

  test('"1" => 1', () => {
    expect(toNumber('10000000000000000000000')).toBe(10000000000000000000000);
  });


  test('"a" is error', () => {
    expect(() => {
      toNumber('a');
    }).toThrow('value \'a\' is not a number!');
  });
});
